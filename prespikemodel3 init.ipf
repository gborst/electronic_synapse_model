#pragma rtGlobals=3		// Use modern global access method and strict wave access.
// Initialization file for prespike models
//   by MCS 2019

// This procedure file is used to initialize the necessary coefficients and contains has basic codes to run the prespike model.
// Which model is run, depends on the model procedure file with ODEs that is compiled together with the initialization procedure file.
//

Function MCS_InitializeCoefs() // generate coefficient and a table to allow easy changing parameters.
	Make/O /N=14 w_coefs
	Make/O /N=14 /T w_coefsnames
	w_Coefsnames[0] = {"c_post in pF"}
	w_Coefsnames[1] = {"g_post in nS"} 
	w_Coefsnames[2] = {"A_app in um^2"}
	w_Coefsnames[3] = {"g_postapp in nS"}
	w_Coefsnames[4] = {"c_m in pF/um^2"}
	w_Coefsnames[5] = {"g_cleft in nS"}
	w_Coefsnames[6] = {"gpre_Na in nS/um^2"}
	w_Coefsnames[7] = {"gpre_KHT in nS/um^2"}
	w_Coefsnames[8] = {"gpre_KLT in nS/um^2"}
	w_Coefsnames[9] = {"gpre_Ca in nS/um^2"}
	w_Coefsnames[10] = {"Rs in MOhms"}
	w_Coefsnames[11] = {"Cp in pF"}
	w_Coefsnames[12] = {"VC mode (0/1)"}
	w_Coefsnames[13] = {"T (degrees Celsius)"}
	w_coefs[0,] = {30, 4, 1000, 0, 0.01, 1000, 0.25, 0.08, 0.24, 0.04,0.1,0.1,1, 32}
	DoWindow/K Coefs_table
	Edit/N=Coefs_table w_Coefsnames, w_Coefs
	
	// channel kinetics
	Make/O /N=12 calyx_Na =    {48.9, 1/128.4,		1.46, -1/8.9,		392.8, 1/36.0,	0.73, -1/15.5,		7.8, 1/35.9,	0.01, -1/18}//Yang et al. 2014
	Make/O /N=8 calyx_HTK =   {1.097,1/57.404,	0.794,-1/79.264,	33.750,0,	74.360,0} // Yang et al. 2014 
	Make/O /N=8 calyx_LTK =   {1.204, 1/37.574, 	0.360,-1/230,		245.488,0,	132.566,0} // Yang et al. 2014
	Make/O /N=4 calyx_Ca = 	   {1.78, 1/23.3, 0.14, -1/15} // Borst et al. 1998
		
	variable/G vpre_rest = -70
	variable/G vpost_rest = -70
	variable/G V_Na = 50
	variable/G V_K = -90
	variable/G V_Ca = 40

End
//--------------------------------------------

Function MCS_DoModel(V_in, w_coefs, [plot_new]) // basic simulation function

	wave V_in, w_coefs
	variable plot_new
	if(ParamIsDefault(plot_new))
		plot_new = 0
	endif
	wave calyx_Na
	wave calyx_HTK
	wave calyx_LTK
	wave calyx_Ca
	variable RC_filter = 1
	variable temp = w_coefs[13]

	Duplicate/O V_in, V_pre
	SetScale/P x, leftx(V_in), deltax(V_in), V_pre
	Duplicate/O v_pre, dvdt_model
	Redimension/N=(-1,21)	dvdt_model
	SetScale/P x, leftx(V_pre), deltax(V_pre), dvdt_model
	
	variable vstart = v_pre[0]
	// presynaptic sodium conductance
	variable Na_alpha = 	MCS_ReturnKinetics(calyx_Na, 0, vstart, temp)
	variable Na_beta = 	MCS_ReturnKinetics(calyx_Na, 1, vstart, temp)
	variable Na_gamma = MCS_ReturnKinetics(calyx_Na, 2, vstart, temp)
	variable Na_delta = 	MCS_ReturnKinetics(calyx_Na, 3, vstart, temp)
	variable Na_theta = 	MCS_ReturnKinetics(calyx_Na, 4, vstart, temp)
	variable Na_epsilon=	MCS_ReturnKinetics(calyx_Na, 5, vstart, temp)

	// presynaptic potassium conductance HT
	variable HTK_alpha = 	MCS_ReturnKinetics(calyx_HTK, 0, vstart, temp)
	variable HTK_beta = 	MCS_ReturnKinetics(calyx_HTK, 1, vstart, temp)
	variable HTK_gamma = MCS_ReturnKinetics(calyx_HTK, 2, vstart, temp)
	variable HTK_delta = 	MCS_ReturnKinetics(calyx_HTK, 3, vstart, temp)

	// presynaptic potassium conductance LT
	variable LTK_alpha = 	MCS_ReturnKinetics(calyx_LTK, 0, vstart, temp)
	variable LTK_beta = 	MCS_ReturnKinetics(calyx_LTK, 1, vstart, temp)
	variable LTK_gamma = MCS_ReturnKinetics(calyx_LTK, 2, vstart, temp)
	variable LTK_delta = 	MCS_ReturnKinetics(calyx_LTK, 3, vstart, temp)
	
	// presynaptic calcium conductance, no LQJ correction in the paper of Lin et al. 2011
	//variable mca_alpha = calyx_Ca[0] *exp(calyx_Ca[1]*(10+v_pre(t)))
	//variable mca_beta    = calyx_Ca[2] *exp(calyx_Ca[3]*(10+v_pre(t))) 
	// presynaptic calcium conductance, Borst et al. 1998
	variable mca_alpha = MCS_ReturnKinetics(calyx_Ca, 0, vstart, temp)
	variable mca_beta    = MCS_ReturnKinetics(calyx_Ca, 1, vstart, temp)
	variable mca_tau    = mca_alpha + mca_beta
		
	dvdt_model[0,][0] = -70 //*10^-3 // V soma
	dvdt_model[0,][1] = 0			// V junction
	//dvdt_model[0,][2] = 0
	dvdt_model[0,][2] = 1
	dvdt_model[0,][3] = 0
	dvdt_model[0,][4] = 0
	dvdt_model[0,][5] = 0
	dvdt_model[0,][6] = 0
	dvdt_model[0,][7] = 1
	dvdt_model[0,][8] = 0
	dvdt_model[0,][9] = 0
	dvdt_model[0,][10] = 0
	dvdt_model[0,][11] = 0
	dvdt_model[0,][12] = 0
	dvdt_model[0,][13] = 1
	dvdt_model[0,][14] = 0
	dvdt_model[0,][15] = 0
	dvdt_model[0,][16] = 0
	dvdt_model[0,][17] = 0
	dvdt_model[0,][18] = 0
	dvdt_model[0,][19] = mca_alpha/mca_tau
	dvdt_model[0,][20] = -70
	
	v_pre = vstart
	Duplicate/O V_pre dvdt_pre
	SetScale/P x, leftx(V_pre), deltax(V_pre), dvdt_pre
	Differentiate dvdt_pre
	
	IntegrateODE/E=(10^-6) /X={leftx(v_pre),deltax(v_pre)} MCS_prespike_model, w_coefs, dvdt_model											
	
	variable lastp = numpnts(v_pre)-1
	dvdt_model[0,][0] = -70 //*10^-3 // V soma
	dvdt_model[0,][1] = 0			// V cleft
	//dvdt_model[0,][2] = 0
	dvdt_model[0,][2] = dvdt_model[lastp][2]
	dvdt_model[0,][3] = dvdt_model[lastp][3]
	dvdt_model[0,][4] = dvdt_model[lastp][4]
	dvdt_model[0,][5] = dvdt_model[lastp][5]
	dvdt_model[0,][6] = dvdt_model[lastp][6]
	dvdt_model[0,][7] = dvdt_model[lastp][7]
	dvdt_model[0,][8] = dvdt_model[lastp][8]
	dvdt_model[0,][9] = dvdt_model[lastp][9]
	dvdt_model[0,][10] = dvdt_model[lastp][10]
	dvdt_model[0,][11] = dvdt_model[lastp][11]
	dvdt_model[0,][12] = dvdt_model[lastp][12]
	dvdt_model[0,][13] = dvdt_model[lastp][13]
	dvdt_model[0,][14] = dvdt_model[lastp][14]
	dvdt_model[0,][15] = dvdt_model[lastp][15]
	dvdt_model[0,][16] = dvdt_model[lastp][16]
	dvdt_model[0,][17] = dvdt_model[lastp][17]
	dvdt_model[0,][18] = dvdt_model[lastp][18]
	dvdt_model[0,][19] = mca_alpha/mca_tau
	dvdt_model[0,][20] = -70
	Duplicate/O V_in, V_pre
	Duplicate/O V_pre dvdt_pre
	SetScale/P x, leftx(V_pre), deltax(V_pre), dvdt_pre
	Differentiate dvdt_pre
	
	IntegrateODE/E=(10^-6) /X={leftx(v_pre),deltax(v_pre)} MCS_prespike_model, w_coefs, dvdt_model
	
	Duplicate/O /R=[0,][0] dvdt_model V_post
	Duplicate/O /R=[0,][1] dvdt_model V_cleft
	Duplicate/O /R=[0,][20] dvdt_model V_rec
	Duplicate/O /R=[0,][5] dvdt_model Na_open
	Duplicate/O /R=[0,][12] dvdt_model KHT_open
	Duplicate/O /R=[0,][18] dvdt_model KLT_open	
	Duplicate/O /R=[0,][19] dvdt_model Ca_m
	Duplicate/O Ca_m Ca_open
	Ca_open[0,] = Ca_m^2

	Duplicate/O Na_open I_Na
	I_Na[0,] = Na_open*w_coefs[6]*w_coefs[2]*(V_pre-V_cleft-50)*10^-3*10^-9
	Duplicate/O KLT_open I_K
	I_K[0,]   = (KHT_open*w_coefs[7]  + KLT_open*w_coefs[8]) *w_coefs[2]* (V_pre-V_cleft + 90)*10^-3*10^-9
	Duplicate/O Ca_open I_Ca
	I_Ca[0,] = Ca_open*w_coefs[9]*w_coefs[2]*(v_pre-v_cleft-40)*10^-3*10^-9 
	Duplicate/O I_Na I_rest
	I_rest[0,] = I_Na + I_Ca - I_K
	
	Duplicate/O V_rec I_rec
	I_rec = 0;
	//I_rec[0,] += I_rest[p] // A
	I_rec[0,] += w_coefs[3] * (V_post[p]- V_cleft[p]+70) *10^-12  // nS * mV = pA
	Duplicate/O V_cleft Vd_cleft
	Differentiate Vd_cleft
	I_rec[0,] -= w_coefs[2]*w_coefs[4]*Vd_cleft[p] *10^-12// pF * V/s = pA
	Duplicate/O V_post Vd_post
	Differentiate Vd_post
	I_rec[0,] += w_coefs[2]*w_coefs[4]*Vd_post*10^-12
	I_rec *= 10^9
	
	Duplicate/O dvdt_pre ddvdt_pre
	Differentiate ddvdt_pre
	ddvdt_pre *= -10^-3
	dvdt_pre *= 10^-3
	
	if(plot_new)
	DoWindow/K Modelresults
	Display/N=Modelresults v_post
	AppendToGraph/L=L_Ipost I_rec
	AppendToGraph/L=L_cleft v_cleft
	AppendToGraph/R=R_pre dvdt_pre
	AppendToGraph/L=L_pre v_pre
	AppendToGraph/R=R_ddvdt ddvdt_pre
	Modifygraph rgb=(0,0,0), rgb(dvdt_pre)=(0,0,65000), rgb(ddvdt_pre)=(0,0,65000)
	AppendToGraph/C=(0,0,65000) V_rec
	AppendToGraph/R=Ch_pre Na_open
	AppendToGraph/R=Ch_pre /C=(0,0,65000) KHT_open
	AppendToGraph/R=Ch_pre /C=(0,65000,0) KLT_open
	AppendToGraph/R=Ch_pre /C=(65000,15000,0) Ca_open
	ModifyGraph standoff(left)=0,standoff(L_cleft)=0,standoff(L_pre)=0, standoff(L_Ipost)=0
	ModifyGraph axisEnab(left)={0,0.28},axisEnab(L_Ipost)={0.29,0.57},axisEnab(L_cleft)={0.59,0.79}
	ModifyGraph axisEnab(L_pre)={0.8,1},freePos(L_cleft)={0,bottom}
	ModifyGraph axisEnab(Ch_pre)={0.8,1},freePos(Ch_pre)={6.0,bottom}
	ModifyGraph axisEnab(R_pre)={0.59,0.79},freePos(R_pre)={6.0,bottom}
	ModifyGraph axisEnab(R_ddvdt)={0.29,0.57},freePos(R_ddvdt)={6.0,bottom}
	ModifyGraph freePos(L_pre)={0,bottom}, freePos(L_Ipost)={0,bottom}
	ModifyGraph lblPos(L_cleft)=59,lblLatPos(L_cleft)=0
	ModifyGraph lblPos(L_pre)=59,lblLatPos(L_pre)=0
	ModifyGraph lblPos(L_Ipost)=59,lblLatPos(L_Ipost)=0
	ModifyGraph lblPos(Ch_pre)=55,lblLatPos(Ch_pre)=0
	ModifyGraph lblPos(R_pre)=55,lblLatPos(R_pre)=0
	ModifyGraph lblPos(R_ddvdt)=55,lblLatPos(R_ddvdt)=0
	
	//ModifyGraph freePos(Ch_pre)={,bottom}
	Label left "V\Bpost\M (mV)"
	Label L_Ipost "I\Bcleft-post\M (nA)"
	Label L_cleft "V\Bjunction\M (mV)"
	Label L_pre "V\Bpre\M (mV)"
	Label Ch_pre "Channel open"
	Label R_pre "\K(0,0,65000)AP\Bcalyx\M (V/ms)"
	Label R_ddvdt "\K(0,0,65000)AP\Bcalyx\M (-V/s\S2\M)"
	SetAxis bottom 0.4 , 2.2
	//Legend/F=0 /A=RT "\\Z05\\s(V_post)\\Z11V\\Bcalyx\\M, V\\Bcleft\\M, V\\Bpost\\M\r\\Z05\\s(Na_open)\\Z11Na\\Bopen\\M\r\\Z05\\s(KHT_open)\\Z1110x KHT\\Bopen\\M\r\\Z05\\s(KLT_open)\\Z11KLT\\Bopen\\M"
	DoWindow/F ModelResults
	SetAxis left -71.5,-68.6
	SetAxis L_Ipost -0.430, 0.860
	SetAxis l_cleft -2.5, 4.5
	SetAxis l_pre -83, 58
	SetAxis Ch_pre 0,0.8
	SetAxis R_pre -0.800,-0.800/-2.5*4.5
	SetAxis R_ddvdt -14.5, -14.5/-0.43*0.86
	Label bottom "time (ms)"
	endif
	
	DoWindow/F ModelResults

End
//-----------------------------------------------------

Function MCS_ReturnKinetics(w_kin, ind, V_p, temp)
	// 0: alpha, 1: beta, 2: gamme, 3: delta, 4: theta, 5:epsilon
	wave w_kin
	variable ind, V_p, temp
	variable kin, Q_ten
	//Q_ten = w_kin[dimsize(w_kin,0)-1]
	Q_ten = 3^(0.1*(temp-20))
	kin = w_kin[ind*2]*exp(V_p*w_kin[ind*2+1])*Q_ten
	return kin
End