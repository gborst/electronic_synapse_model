# README #

### What is this repository for? ###

*Files associated with Sierksma and Borst PLOS Computational Biology "Using ephaptic coupling to estimate the synaptic cleft resistivity of the calyx of Held synapse".

### How do I get set up? ###

*The Igor Pro experiment files (.pxp) can be used to recreate figures. Igor Pro (https://www.wavemetrics.com) is needed for this.

*The Igor Pro procedure files (.ipf) are txt files that can be opened with any text editor.

*'Figure 5 PLOS.pxp' and 'Figure 6 PLOS.pxp' both need procedure files 'prespikemodel3.ipf' and 'prespikemodel3 init.ipf'.
