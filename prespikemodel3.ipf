#pragma rtGlobals=3		// Use modern global access method and strict wave access.
// Model for prespike
// by MCS 2018
// This procedure file contains a prespike model based on ordinary differential equations for an electrical circuit with voltage-gated channels.

// Input is a CC waveform of the calyx
//           and parameters of the circuit
// Outputs are waveforms of the junction potential and the postsynaptic potential
//
// Model includes capacitive and passive resistive and capacitive coupling between the junction and postsynaptic cell, and coupling via Kv1-like, Kv3-like, Ca-like and Na-like conductances between calyx and junction.
//
// first initialize with MCS_MakeTable(); MCS_Make_preAP()
// then run MCS_DoModel(APmodel, w_coefs, plot_new=1)
// Adjustments in parameters can be made in Coefs_table followed by MCS_DoModel(APmodel, w_coefs, plot_new=0)
// You can also use a recorded waveform: load your AP and adjust if necessary (see MCS_Load_recAP() for my adjustments).

Function MCS_prespike_model(w_coefs, t, v, dvdt)
	// Inputs
	wave w_coefs
	variable t // in ms
	wave v
	wave dvdt
	
	// initialize
	variable VC_mode = w_coefs[12]
	wave dvdt_pre
	wave v_pre
	wave calyx_Na
	wave calyx_HTK
	wave calyx_LTK
	wave calyx_Ca

	NVAR vpre_rest 
	NVAR vpost_rest
	NVAR V_Na
	NVAR V_K
	NVAR V_Ca
	variable c_post = w_coefs[0] * 10^-12 // pF -> F
	variable g_post = w_coefs[1] * 10^-9 // nS -> S
	variable A_postapp = w_coefs[2]	 // um^2
	variable g_postapp = w_coefs[3] *10^-9 // nS -> S
	variable c_m = w_coefs[4]*10^-12         // pF/um^2 -> F/um^2 
	variable g_cleft = w_coefs[5] *10^-9  // nS -> S
	variable A_preapp = w_coefs[2]        // um^2
	//variable c_preapp = w_coefs[6] * 10^-12 // pF
	variable g_Na = w_coefs[6] * w_coefs[2] *10^-9 // nS/um^2 * um^2- >S 
	variable g_KHT = w_coefs[7] * w_coefs[2] *10^-9 // nS/um^2 * um^2- >S 
	variable g_KLT = w_coefs[8] * w_coefs[2] *10^-9 // nS/um^2 * um^2- >S 
	variable g_Ca   = w_coefs[9] * w_coefs[2] *10^-9 // nS/um^2 * um^2- >S 
	variable Rs = w_coefs[10] * 10^6
	variable Cp = w_coefs[11] * 10^-12
	variable temp = w_coefs[13]
	
	if( t != max(min(t, rightx(V_pre)), leftx(V_pre)))
		t = max(min(t, rightx(V_pre)), leftx(V_pre))
	endif

	// presynaptic sodium conductance
	if(g_Na > 0)
		variable Na_alpha = 	MCS_ReturnKinetics(calyx_Na, 0, v_pre(t)-v[1], temp)
		variable Na_beta = 	MCS_ReturnKinetics(calyx_Na, 1, v_pre(t)-v[1], temp)
		variable Na_gamma = MCS_ReturnKinetics(calyx_Na, 2, v_pre(t)-v[1], temp)
		variable Na_delta = 	MCS_ReturnKinetics(calyx_Na, 3, v_pre(t)-v[1], temp)
		variable Na_theta = 	MCS_ReturnKinetics(calyx_Na, 4, v_pre(t)-v[1], temp)
		variable Na_epsilon=	MCS_ReturnKinetics(calyx_Na, 5, v_pre(t)-v[1], temp)
	endif
	variable Na_open = v[5]

	// presynaptic potassium conductance HT
	if(g_KHT > 0)
		variable HTK_alpha = 	MCS_ReturnKinetics(calyx_HTK, 0, v_pre(t)-v[1], temp)
		variable HTK_beta = 	MCS_ReturnKinetics(calyx_HTK, 1, v_pre(t)-v[1], temp)
		variable HTK_gamma = MCS_ReturnKinetics(calyx_HTK, 2, v_pre(t)-v[1], temp)
		variable HTK_delta = 	MCS_ReturnKinetics(calyx_HTK, 3, v_pre(t)-v[1], temp)
	endif
	variable KHT_open = v[12]

	// presynaptic potassium conductance LT
	if(g_KLT>0)
		variable LTK_alpha = 	MCS_ReturnKinetics(calyx_LTK, 0, v_pre(t)-v[1], temp)
		variable LTK_beta = 	MCS_ReturnKinetics(calyx_LTK, 1, v_pre(t)-v[1], temp)
		variable LTK_gamma = MCS_ReturnKinetics(calyx_LTK, 2, v_pre(t)-v[1], temp)
		variable LTK_delta = 	MCS_ReturnKinetics(calyx_LTK, 3, v_pre(t)-v[1], temp)
	endif
	variable KLT_open = v[18]
	
	// presynaptic calcium conductance, Borst et al. 1998
	if(g_Ca>0)
		variable mca_alpha = MCS_ReturnKinetics(calyx_Ca, 0, v_pre(t)-v[1], temp)
		variable mca_beta    = MCS_ReturnKinetics(calyx_Ca, 1, v_pre(t)-v[1], temp)
		variable mca_tau    = mca_alpha + mca_beta
	endif
	variable Ca_open    = v[19]^2
	
	variable dvdt_0 = dvdt[0]
	variable dvdt_1 = dvdt[1]
	variable i = 1
	
	//quick fix for when NaN's arise
	if(numtype(dvdt[0]) != 0)
		dvdt[0] = 0
	endif
	if(numtype(dvdt[1]) !=0)
		dvdt[1] = 0
	endif
	
	for(i=1;i<10;i=i+1)
		if(VC_mode)
			dvdt[0] = 0;
		else
			dvdt[0] = 1/(c_post)*(c_m*A_postapp*dvdt[1] - 10^-3*g_postapp*(v[0]-v[1]) - 10^-3*(g_post)*(v[0] - vpost_rest))
		endif
		dvdt[1] = 1/(c_m*A_postapp+c_m*A_preapp) *(c_m*A_preapp*dvdt_pre(t) +c_m*A_postapp*dvdt[0] - 10^-3*g_cleft*v[1] + 10^-3*g_postapp*(v[0] - v[1] + vpost_rest) + 10^-3*g_Na*Na_open*(v_pre(t)-v[1]-V_Na) + 10^-3*(g_KHT*KHT_open+g_KLT*KLT_open)*(v_pre(t)-v[1]-V_K) + 10^-3*g_Ca*Ca_open*(v_pre(t)-v[1] - V_Ca))
	endfor
	
	dvdt[2,19] = 0
	// sodium states - 5 - C0-C1-C2-O-I
	if(g_Na > 0)
		dvdt[2] = -v[2]*(2*Na_alpha + 0*Na_beta)		+   v[3]*1*Na_beta
		dvdt[3] = -v[3]*(1*Na_alpha + 1*Na_beta)		+   v[2]*2*Na_alpha + v[4]*2*Na_beta
		dvdt[4] = -v[4]*(2*Na_beta + 1*Na_gamma)	+   v[3]*1*Na_alpha + v[5]*1*Na_delta
		dvdt[5] = -v[5]*(1*Na_delta + 1*Na_theta)	+   v[4]*1*Na_gamma + v[6]*1*Na_epsilon	
		dvdt[6] = -v[6]*(1*Na_epsilon)				+   v[5]*1*Na_theta
	endif
	
	// HTK states - 6 - C0-C1-C2-C3-C4-O
	if(g_KHT > 0)
		dvdt[7]   = -v[7]*(4*HTK_alpha + 0*HTK_beta)		+                                v[8]*1*HTK_beta
		dvdt[8]   = -v[8]*(3*HTK_alpha + 1*HTK_beta)		+   v[7]*4*HTK_alpha + v[9]*2*HTK_beta
		dvdt[9]   = -v[9]*(2*HTK_alpha + 2*HTK_beta)		+   v[8]*3*HTK_alpha + v[10]*3*HTK_beta
		dvdt[10] = -v[10]*(1*HTK_alpha + 3*HTK_beta)		+   v[9]*2*HTK_alpha + v[11]*4*HTK_beta
		dvdt[11] = -v[11]*(1*HTK_gamma + 4*HTK_beta)	+   v[10]*1*HTK_alpha + v[12]*1*HTK_delta
		dvdt[12] = -v[12]*(1*HTK_delta + 0*HTK_beta)		+   v[11]*1*HTK_gamma 
	endif
	
	// LTK states - 6 - C0-C1-C2-C3-C4-O
	if(g_KLT > 0)
		dvdt[13]   = -v[13]*(4*LTK_alpha + 0*LTK_beta)		+                                v[14]*1*LTK_beta
		dvdt[14]   = -v[14]*(3*LTK_alpha + 1*LTK_beta)		+   v[13]*4*LTK_alpha + v[15]*2*LTK_beta
		dvdt[15]   = -v[15]*(2*LTK_alpha + 2*LTK_beta)		+   v[14]*3*LTK_alpha + v[16]*3*LTK_beta
		dvdt[16] = -v[16]*(1*LTK_alpha + 3*LTK_beta)		+   v[15]*2*LTK_alpha + v[17]*4*LTK_beta
		dvdt[17] = -v[17]*(1*LTK_gamma + 4*LTK_beta)	+   v[16]*1*LTK_alpha + v[18]*1*LTK_delta
		dvdt[18] = -v[18]*(1*LTK_delta + 0*LTK_beta)		+   v[17]*1*LTK_gamma 
	endif

	// Ca
	if(g_Ca > 0)
		dvdt[19] = (mca_alpha/mca_tau-v[19])*mca_tau
	endif
	
	// Vrec VC
	if(VC_mode)
		dvdt[20] = 0
		//v[20] = c_m*A_postapp*dvdt[1] - 10^-3*g_postapp*(v[0]-v[1]-vpost_rest) - 10^-3*(g_post-g_postapp)*(v[0] - vpost_rest)
	else
	// Vrec CC
		dvdt[20] = (v[0] - v[20])*10^-3/(Rs*Cp)
	endif

	return 0
End
//-----------------------------------------------

Function MCS_Make_PreAP()
	Make/O /N=701 APmodel = -70
	SetScale/I x,0,0.7, APmodel
	variable p_offset = 200
	variable amp = 140	
	APmodel[p_offset,50+p_offset] = -70 + amp*(p-p_offset)/50
	APmodel[50+p_offset,50+p_offset+200] = -70+amp - amp/200*(p-50-p_offset)
	Smooth/B=15 30, APmodel
End
//----------------------------------

Function MCS_Load_recAP()
	// MCS AP	
	// load APavg from 20161003 P08 APavg.ibw
	wave APavg
	if(WaveExists(APavg))
	SetScale/P x,0,0.02, APavg
	Duplicate/O APavg APavgadj
	APavgadj +=74; APavgadj/=85; APavgadj*=100; APavgadj-=74;
	SetScale/P x,0,0.02,APavgadj
	Duplicate/O /R=(4,7) APavgadj AP_short
	SetScale/P x,0,0.02, AP_short
	AP_short[0,34] = -73.2
	AP_short[67,] = -73.2
	Duplicate/O AP_short, AP_short_orig
	Duplicate/O AP_short AP_high
	AP_high-=AP_short[0]
	AP_high/=98.1664
	AP_high*=125
	AP_high+= AP_short[0]
	endif
	
	//GB AP
	wave first_AP_train_CS, last_AP_train_Cs
	if(WaveExists(first_AP_train_CS))
	Duplicate/O /R=[350, 500]  first_AP_train_CS APGB_first
	Duplicate/O /R=[350, 500] last_AP_train_CS APGB_last
	SetScale/P x,0, 0.02, APGB_first, APGB_last
	APGB_first[0,] -= 80
	APGB_last[0,] -= 80
	Make/O /N=(2*dimsize(APGB_first,0)) APGB_Ca = NaN
	SetScale/P x,0,0.01, APGB_Ca
	Interpolate2/Y=APGB_Ca /E=2 APGB_first
	Duplicate/O APGB_Ca APGB_Cad1
	Differentiate APGB_Cad1
	WaveStats/Q/Z APGB_Cad1
	SetScale/P x,-V_maxloc+1,deltax(APGB_Ca), APGB_Ca
	endif
	
	wave fifty_AP_eq_CS, fifty_AP_uneq_CS
	if(WaveExists(fifty_AP_eq_CS))
	// only use the last 30
	Duplicate/O /R=[9650,] fifty_AP_eq_CS APGB_Ca_traineq
	Duplicate/O /R=[9650,] fifty_AP_uneq_CS APGB_Ca_trainuneq	
	SetScale/P x,0,0.02, APGB_Ca_traineq, APGB_Ca_trainuneq	
	APGB_Ca_traineq -=80
	APGB_Ca_trainuneq -= 80
	endif
	
End
//---------------------



	
	



	